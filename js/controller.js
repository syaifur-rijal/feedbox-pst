// controller untuk sidemenu
app.controller('MenuCtrl', function() {
    this.load = function(page) {
        myMenu.content.load(page).then(function() {
            myMenu.left.close();
        });
    };
});

// controller untuk home
app.controller('HomeCtrl', ['$scope', function($scope) {
    ons.ready(function() {
        ons.createPopover('popover.html').then(function(popover) {
            $scope.popover = popover;
        });
    });    
}]);


app.controller('PopoverCtrl', ['$scope', 'DatabaseService', function($scope, DatabaseService) {
    $scope.reset = function() {
        ons.notification.confirm({
            title: 'Reset Data', 
            message: 'Apakah Anda Ingin Menghapus Semua Data ?',
            buttonLabels: ['Ya', 'Tidak'],
            animation: 'default',
            primaryButtonIndex: 1,
            cancelable: true,
            callback: function(index) {
                if(index != 1) {
                    DatabaseService.resetDB();
                    ons.notification.alert({ title: 'Info', message: 'Data Berhasil Dihapus' });
                }
            } 
        });
    };
}]);

// controller untuk template survey
app.controller('SurveyCtrl', ['$scope', 'DatabaseService', 'GetDateService', function($scope, DatabaseService, GetDateService) {
    
    $scope.save = function(kategori) {

        switch(kategori) {
            case 'Sangat Puas' :
                DatabaseService.setData("INSERT INTO SV_SGTPUAS VALUES(?,?)", [kategori, GetDateService.currentDate()]);
                DatabaseService.setData("INSERT INTO SURVEY VALUES(?,?)", [kategori, GetDateService.currentDate()]);
                break;
            case 'Puas' :
                DatabaseService.setData("INSERT INTO SV_PUAS VALUES(?,?)", [kategori, GetDateService.currentDate()]);
                DatabaseService.setData("INSERT INTO SURVEY VALUES(?,?)", [kategori, GetDateService.currentDate()]);
                break;
            case 'Tidak Puas' :
                DatabaseService.setData("INSERT INTO SV_TDKPUAS VALUES(?,?)", [kategori, GetDateService.currentDate()]);
                DatabaseService.setData("INSERT INTO SURVEY VALUES(?,?)", [kategori, GetDateService.currentDate()]);
        }

        ons.notification.alert({
            title: 'INFO',
            message: 'Terima Kasih Atas Penilaian Anda'
        });

        this.myRootNavigator.pushPage('menu.html', {animation: 'fade'});
    };
}]);

// controller untuk feedback pengguna
app.controller('ListKomentarCtrl', ['$scope', '$q', 'DatabaseService', function($scope, $q, DatabaseService) {
    var survey = [];
    var deferred = $q.defer();

    DatabaseService.getData("SELECT nama, komentar, strftime('%d/%m/%Y', tg_komentar) AS tanggal FROM KOMENTAR").then(function(data) {
        survey = data;
        deferred.resolve(survey);
    }); 

    $scope.delegate = {
        configureItemScope : function(index, itemScope) {
            deferred.promise.then(
                function success(data) {
                    itemScope.item = {
                        // orang: 'Pengunjung ' + (index + 1),
                        orang: survey[index]['nama'] != 'undefined' ? survey[index]['nama'] : 'Pengunjung Anonim',
                        komentar: survey[index]['komentar'],
                        t_kunjungan: survey[index]['tanggal']
                    };
                }, function error(err) {
                    console.log(err);
                }
            );
        },
        countItems: function() { 
            // menghitung jumlah list
            return survey.length; 
        },
        calculateItemHeight: function() {
            // menampilkan list nama maximal 100 
            return 94; 
        } 
    };
}]);

// controller entri kritik dan saran
app.controller('KomentarCtrl', ['$scope', 'DatabaseService', 'GetDateService', function($scope, DatabaseService, GetDateService) {

    $scope.save = function(nama, komentar) {
    
        DatabaseService.setData("INSERT INTO KOMENTAR VALUES(?,?,?)", [nama, komentar, GetDateService.currentDate()]);
    
        ons.notification.alert({ title: 'Info', message: 'Terima Kasih Atas Kritik dan Saran Anda' });

        this.myRootNavigator.resetToPage('menu.html', {animation:'fade'});   
    };
}]);

// controller jumlah pengunjung dalam hari
app.controller('JmlPengunjungHariCtrl', ['$scope', '$q', 'DatabaseService', 'GetDateService', function($scope, $q, DatabaseService, GetDateService) {
    
    var D_SangatPuas = [];
    var D_Puas = [];
    var D_TidakPuas = [];

    // async for jumlah pengunjung dalam sehari
    var dfTotal = $q.defer();

    // async for jumlah pengunjung sangat puas
    var dfSangatPuas = $q.defer();

    // async for jumlah pengunjung puas
    var dfPuas = $q.defer();

    // async for jumlah pengunjung tidak puas
    var dfTidakPuas = $q.defer();

    $scope.labels = [GetDateService.currentBeautyDate()];
    $scope.colors = ['#3498db', '#2ecc71', '#e74c3c'];
    $scope.series = ['Sangat Puas', 'Puas', 'Tidak Puas'];

    // query jumlah pengunjung sangat puas dalam satu hari
    DatabaseService.getData("SELECT COUNT(kategori) AS kategori FROM SURVEY WHERE STRFTIME('%Y-%m-%d', tg_survey) = '" + GetDateService.currentDate() + "'").then(function(data) {
        dfTotal.resolve(data);
    });

    dfTotal.promise.then(
        function success(data) {
            $scope.options = {
                scales: { 
                    yAxes: [{ ticks: { beginAtZero: true } }]
                },
                title: {
                    display: true,
                    text: 'Jumlah Pengunjung Pelayanan Statistik Terpadu Tanggal ' + GetDateService.currentBeautyDate() + ' Sebanyak ' + data[0]['kategori'] + ' Kunjungan',
                    position: 'top',
                    fontStyle: 'bold',
                    fontSize: 17,
                    padding: 20
                }
            };
        }, function error(err) { console.log(err); }
    );

    // query jumlah pengunjung sangat puas pada bulan berjalan
    DatabaseService.getData("SELECT COUNT(kategori) AS kategori FROM SV_SGTPUAS WHERE STRFTIME('%Y-%m-%d', tg_survey) = '" + GetDateService.currentDate() + "'").then(function(data) {
        dfSangatPuas.resolve(data);
    });

    dfSangatPuas.promise.then(
        function success(data) {
            D_SangatPuas.push(data[0]['kategori']);
        }, function error(err) { console.log(err); }
    );

    // query jumlah pengunjung puas pada bulan berjalan
    DatabaseService.getData("SELECT COUNT(kategori) AS kategori FROM SV_PUAS WHERE STRFTIME('%Y-%m-%d', tg_survey) = '" + GetDateService.currentDate() + "'").then(function(data) {
        dfPuas.resolve(data);
    });

    dfPuas.promise.then(
        function success(data) {
            D_Puas.push(data[0]['kategori']);
        }, function error(err) { console.log(err); }
    );                   

    DatabaseService.getData("SELECT COUNT(kategori) AS kategori FROM SV_TDKPUAS WHERE STRFTIME('%Y-%m-%d', tg_survey) = '" + GetDateService.currentDate() + "'").then(function(data) {
        dfTidakPuas.resolve(data);
    });

    dfTidakPuas.promise.then(
        function success(data) {
            D_TidakPuas.push(data[0]['kategori']);
        }, function error(err) { console.log(err); }
    );

    $scope.data = [
        D_SangatPuas, D_Puas, D_TidakPuas
    ];    
}]);

// controller jumlah pengunjung dalam bulan
app.controller('JmlPengunjungBulanCtrl', ['$scope', '$q', 'DatabaseService', 'GetDateService', function($scope, $q, DatabaseService, GetDateService) {

    var D_SangatPuas = [];
    var D_Puas = [];
    var D_TidakPuas = [];

    // async for jumlah pengunjung total
    var dfTotal = $q.defer();


dfTotal.promise.then(
    function success(data) {

    }, function error(err) { consoe.log(err); }
);
    // async for nama bulan
    var dfNamaBulan = $q.defer();

    // async for pengunjung sangat puas
    var dfSangatPuas = $q.defer();

    // async for pengunjung puas
    var dfPuas = $q.defer();

    // async for pengunjung tidak puas
    var dfTidakPuas = $q.defer();

    // query mendapatkan bulan berjalan
    DatabaseService.getData("SELECT nama from Bulan WHERE kode = '" + GetDateService.currentMonth() + "'").then(function(bulan) {
        dfNamaBulan.resolve(bulan);
    });

    // query mendapatkan jumlah pengunjung bulanan
    DatabaseService.getData("SELECT COUNT(kategori) AS kategori FROM SURVEY WHERE STRFTIME('%Y-%m', tg_survey) = '" + GetDateService.currentYearMonth() + "'").then(function(data) {
        dfTotal.resolve(data);
    });

    dfNamaBulan.promise.then(
        function success(bulan) {
            $scope.labels  = [bulan[0]['nama']];
            $scope.colors  = ['#3498db', '#2ecc71', '#e74c3c'];
            $scope.series  = ['Sangat Puas', 'Puas', 'Tidak Puas'];

            dfTotal.promise.then(
                function success(data) {
                    $scope.options = {
                        scales: { 
                            yAxes: [{ ticks: { beginAtZero: true } }]
                        },
                        title: {
                            display: true,
                            text: 'Jumlah Pengunjung Pelayanan Statistik Terpadu Bulan ' + bulan[0]['nama'] + ' Tahun ' + GetDateService.currentYear() + ' Sebanyak ' + data[0]['kategori'] + ' Kunjungan',
                            position: 'top',
                            fontStyle: 'bold',
                            fontSize: 17,
                            padding: 20
                        }
                    };
                }, function error(err) { console.log(err); }
            );
        }, function error(err) { console.log(err); }
    );
     
    // query jumlah pengunjung sangat puas pada bulan berjalan
    DatabaseService.getData("SELECT COUNT(kategori) AS kategori FROM SV_SGTPUAS WHERE STRFTIME('%Y-%m', tg_survey) = '" + GetDateService.currentYearMonth() + "'").then(function(data) {
        dfSangatPuas.resolve(data);
    });

    dfSangatPuas.promise.then(
        function success(data) {
            D_SangatPuas.push(data[0]['kategori']);
        }, function error(err) { console.log(err); }
    );

    // query jumlah pengunjung puas pada bulan berjalan
    DatabaseService.getData("SELECT COUNT(kategori) AS kategori FROM SV_PUAS WHERE STRFTIME('%Y-%m', tg_survey) = '" + GetDateService.currentYearMonth() + "'").then(function(data) {
        dfPuas.resolve(data);
    });

    dfPuas.promise.then(
        function success(data) {
            D_Puas.push(data[0]['kategori']);
        }, function error(err) { console.log(err); }
    );                   

    DatabaseService.getData("SELECT COUNT(kategori) AS kategori FROM SV_TDKPUAS WHERE STRFTIME('%Y-%m', tg_survey) = '" + GetDateService.currentYearMonth() + "'").then(function(data) {
        dfTidakPuas.resolve(data);
    });

    dfTidakPuas.promise.then(
        function success(data) {
            D_TidakPuas.push(data[0]['kategori']);
        }, function error(err) { console.log(err); }
    );

    $scope.data = [
        D_SangatPuas, D_Puas, D_TidakPuas
    ];    
}]);

// controller jumlah pengunjung dalam setahun
app.controller('JmlPengunjungTahunCtrl', ['$scope', '$q', 'DatabaseService', 'GetDateService', function($scope, $q, DatabaseService, GetDateService){

    var D_SangatPuas = [];
    var D_Puas = [];
    var D_TidakPuas = [];
    var D_Total;

    // async for jumlah pengunjung sangat puas
    var dfSangatPuas = $q.defer();
    
    // async for jumlah pengunjung puas
    var dfPuas = $q.defer();

    // async for jumlah pengunjung tidak puas
    var dfTidakPuas = $q.defer();

    // async for jumlah pengunjung
    var dfTotal = $q.defer();

    $scope.labels  = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    $scope.colors  = ['#3498db', '#2ecc71', '#e74c3c'];
    $scope.series  = ['Sangat Puas', 'Puas', 'Tidak Puas'];

    // query jumlah pengunjung total
    DatabaseService.getData("SELECT COUNT(kategori) AS kategori FROM SURVEY").then(function(data) {
        dfTotal.resolve(data);
    });

    dfTotal.promise.then(
        function success(data) {
            $scope.options = {
                scales: {
                    yAxes: [{ ticks: { beginAtZero: true } }]
                },
                title: {
                    display: true,
                    text: 'Jumlah Pengunjung Pelayan Statistik Terpadu Tahun ' + GetDateService.currentYear() + ' Sebanyak ' + data[0]['kategori'] + ' Kunjungan',
                    position: 'top',
                    fontStyle: 'bold',
                    fontSize: 17,
                    padding: 20
                }
            };
        }, function error(err) {console.log(err); }
    );

    // query jumlah pengunjung sangat puas
    DatabaseService.getData("SELECT kode, COUNT(SV_SGTPUAS.kategori) AS kategori FROM BULAN LEFT JOIN SV_SGTPUAS ON BULAN.kode = STRFTIME('%m', SV_SGTPUAS.tg_survey) GROUP BY BULAN.kode").then(function(data) {
        dfSangatPuas.resolve(data);
    });

    dfSangatPuas.promise.then(
        function success(data) { 
            for(var i = 0; i < data.length; i++) {
                D_SangatPuas.push(data[i]['kategori']);
            }
        }, function error(err) { console.log(err); }
    );

    // query jumlah pengunjung puas
    DatabaseService.getData("SELECT kode, COUNT(SV_PUAS.kategori) AS kategori FROM BULAN LEFT JOIN SV_PUAS ON BULAN.kode = STRFTIME('%m', SV_PUAS.tg_survey) GROUP BY BULAN.kode").then(function(data) {
        dfPuas.resolve(data);
    });

    dfPuas.promise.then(
        function success(data) { 
            for(var i = 0; i < data.length; i++) {
                D_Puas.push(data[i]['kategori']); 
            }
        }, function error(err) { console.log(err); }
    );

    // query jumlah pengunjung tidak puas
    DatabaseService.getData("SELECT kode, COUNT(SV_TDKPUAS.kategori) AS kategori FROM BULAN LEFT JOIN SV_TDKPUAS ON BULAN.kode = STRFTIME('%m', SV_TDKPUAS.tg_survey) GROUP BY BULAN.kode").then(function(data) {
        dfTidakPuas.resolve(data);
    });

    dfTidakPuas.promise.then(
        function success(data) { 
            for(var i=0; i < data.length; i++) {
                D_TidakPuas.push(data[i]['kategori']);
            }
        }, function error(err) { console.log(err); }
    );   
    
    $scope.data = [
        D_SangatPuas, D_Puas, D_TidakPuas
    ];
}]);

// controller tentang aplikasi
app.controller('TentangCtrl', ['$scope', function($scope) {
    
    $scope.rate = function() {};

    $scope.data = {
        pembuat : "Syaifur Rijal Syamsul",
        email   : "syaifur.rijal@bps.go.id"
    };
}]);