// service untuk share variabel antar controller
app.service('SharedService', function() {
    var data = null;
    return {
        getData: function() { return data; },
        setData: function(value) { data = value; }
    };
});

// service untuk database
app.service('DatabaseService', ['$q', function($q) {
    var db = window.openDatabase('feedbox_db', '1.0', 'Database Feedbox', 5 * 1024 * 1024);

    return {
        // fungsi konfigurasi database
        configDB: function() {
            db.transaction(function(tx) {
                // tabel referensi nama bulan
                tx.executeSql("CREATE TABLE IF NOT EXISTS BULAN (kode INTEGER PRIMARY KEY, nama STRING)");
                tx.executeSql("INSERT INTO BULAN VALUES(1, 'Januari')");
                tx.executeSql("INSERT INTO BULAN VALUES(2, 'Februari')");
                tx.executeSql("INSERT INTO BULAN VALUES(3, 'Maret')");
                tx.executeSql("INSERT INTO BULAN VALUES(4, 'April')");
                tx.executeSql("INSERT INTO BULAN VALUES(5, 'Mei')");
                tx.executeSql("INSERT INTO BULAN VALUES(6, 'Juni')");
                tx.executeSql("INSERT INTO BULAN VALUES(7, 'Juli')");
                tx.executeSql("INSERT INTO BULAN VALUES(8, 'Agustus')");
                tx.executeSql("INSERT INTO BULAN VALUES(9, 'September')");
                tx.executeSql("INSERT INTO BULAN VALUES(10, 'Oktober')");
                tx.executeSql("INSERT INTO BULAN VALUES(11, 'November')");
                tx.executeSql("INSERT INTO BULAN VALUES(12, 'Desember')");

                // tabel survey
                tx.executeSql("CREATE TABLE IF NOT EXISTS SV_SGTPUAS(kategori STRING, tg_survey DATE)");
                tx.executeSql("CREATE TABLE IF NOT EXISTS SV_PUAS(kategori STRING, tg_survey DATE)");
                tx.executeSql("CREATE TABLE IF NOT EXISTS SV_TDKPUAS(kategori STRING, tg_survey DATE)");
                tx.executeSql("CREATE TABLE IF NOT EXISTS SURVEY(kategori STRING, tg_survey DATE)");

                // tabel kotak saran
                tx.executeSql("CREATE TABLE IF NOT EXISTS KOMENTAR (nama STRING, komentar TEXT, tg_komentar DATE)");
            });
        },
        // fungsi untuk mereset isi database
        resetDB: function() {
            db.transaction(function(tx) {
                tx.executeSql("DROP TABLE SV_SGTPUAS");
                tx.executeSql("DROP TABLE SV_PUAS");
                tx.executeSql("DROP TABLE SV_TDKPUAS");
                tx.executeSql("DROP TABLE KOMENTAR");
                tx.executeSql("DROP TABLE SURVEY");
                tx.executeSql("CREATE TABLE IF NOT EXISTS SURVEY(kategori STRING, tg_survey DATE)");
                tx.executeSql("CREATE TABLE IF NOT EXISTS SV_SGTPUAS(kategori STRING, tg_survey DATE)");
                tx.executeSql("CREATE TABLE IF NOT EXISTS SV_PUAS(kategori STRING, tg_survey DATE)");
                tx.executeSql("CREATE TABLE IF NOT EXISTS SV_TDKPUAS(kategori STRING, tg_survey DATE)");
                tx.executeSql("CREATE TABLE IF NOT EXISTS KOMENTAR (nama STRING, komentar TEXT, tg_komentar DATE)");
            });
        },
        // fungsi mengambil data dari database
        getData: function(sql) {
            var deferred = $q.defer();
            db.transaction(function(tx) {
                tx.executeSql(sql, [], function (tx, results) {
                    var i, tab = [];
                    for (i = 0; i < results.rows.length; i++) {
                        tab.push(results.rows.item(i))
                    }
                    deferred.resolve(tab);
                });
            });
            return deferred.promise;
        },
        // fungsi menyimpan data ke database
        setData: function(sql, value) {
            db.transaction(function(tx) {
                tx.executeSql(sql, value);
            });
        }
    };
}]);

// service untuk mendapatkan format tanggal sistem
app.service('GetDateService', function() {
    return {
        currentDate: function() {
            var date = new Date();
        
            var t_kunjungan =  date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2)  + '-' + ('0' + date.getDate()).slice(-2);
            
            return t_kunjungan;
        },
        currentBeautyDate: function() {
            var date = new Date();

            var t_kunjungan = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();

            return t_kunjungan;
        },
        currentMonth: function() {
            var date = new Date();

            var t_kunjungan = ('0' + (date.getMonth() + 1)).slice(-2);

            return t_kunjungan;
        },
        currentYearMonth: function() {
            var date = new Date();

            var t_kunjungan = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2);

            return t_kunjungan;
        },
        currentYear: function() {
            var date = new Date();

            var t_kunjungan = date.getFullYear();

            return t_kunjungan;
        }
    }
});