//  definisikan modul myApp
var app = angular.module('myApp', ['onsen', 'chart.js']);

// inisialisasi aplikasi
app.run(function(DatabaseService) {
    ons.ready(function() {
        DatabaseService.configDB();
    });    
});