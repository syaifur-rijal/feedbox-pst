# Feedbox - PST

Feedbox-PST adalah aplikasi yang bertujuan untuk mendapatkan tanggapan pengunjung PST mengenai pelayanan yang diberikan oleh Pelayanan Statistik Terpadu BPS Provinsi Sulawesi Barat.

Dibuat menggunakan perangkat lunak terbuka, aplikasi ini ditargetkan untuk sistem operasi Android 4.0+ dengan dimensi 1280x800 px, dengan memakai konsep Hybrid App [Onsen UI 2](https://onsen.io/) dan [AngularJS](https://angularjs.org/)